package gohealth.code.challenge.test;

import gohealth.code.challenge.src.WhiteElephant;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TestWhiteElephant {

    private WhiteElephant fixture;

    @Before
    public void setUp() throws Exception{
        fixture = new WhiteElephant();
    }

    @After
    public void tearDown() throws Exception{
        fixture = null;
    }

    /**
     * Testing program by providing null Participant list
     */
    @Test
    public void testNullParticipants(){
        String[] participants = null;

        // Random Generator approach
        assertNull("Null Participants, so no Assignments", fixture.generateAssignmentsUsingRandom(participants));

        // Candidate Set approach
        assertNull("Null Participants, so no Assignments", fixture.generateAssignmentsUsingSet(participants));
    }

    /**
     * No Participants
     */
    @Test
    public void testEmptyParticipants(){
        String[] participants = {};

        // Random Generator approach
        assertNull("No Participants, so no Assignments", fixture.generateAssignmentsUsingRandom(participants));

        // Candidate Set approach
        assertNull("No Participants, so no Assignments", fixture.generateAssignmentsUsingSet(participants));
    }

    /**
     * Only one Participant
     */
    @Test
    public void testSingleParticipant(){
        String[] participants = {"Kyle"};

        // Random Generator approach
        assertNull("Single Participant, so no available Assignment", fixture.generateAssignmentsUsingRandom(participants));

        // Candidate Set approach
        assertNull("Single Participant, so no available Assignment", fixture.generateAssignmentsUsingSet(participants));
    }

    /**
     * Only two Participants
     */
    @Test
    public void testTwoParticipants(){
        String[] participants = {"Kyle", "Kenny"};
        String[] actual;
        String[] expected = {"Kenny", "Kyle"};

        // Random Generator approach
        actual = fixture.generateAssignmentsUsingRandom(participants);
        assertNotNull("Two Participants game should yield a possible Assignment", actual);
        assertTrue("Valid Assignments as it is expected", Arrays.equals(actual, expected));

        // Candidate Set approach
        actual = fixture.generateAssignmentsUsingSet(participants);
        assertNotNull("Two Participants game should yield a possible Assignment", actual);
        assertTrue("Valid Assignments as it is expected", Arrays.equals(actual, expected));
    }

    /**
     * More Participants, odd number
     */
    @Test
    public void testThreeOrMoreOddNumberedParticipants(){
        String[] participants = {"A", "B", "C", "D", "E"};
        String[] assignments;

        /**
         * Random Generator approach
         */
        assignments = fixture.generateAssignmentsUsingRandom(participants);
        assertNotNull("Three or more Participants game should yield a possible Assignments", assignments);
        assertTrue(participants[0] == "A");
        assertTrue(assignments[0]  != "A");
        assertTrue(participants[1] == "B");
        assertTrue(assignments[1]  != "B");
        assertTrue(participants[2] == "C");
        assertTrue(assignments[2]  != "C");
        assertTrue(participants[3] == "D");
        assertTrue(assignments[3]  != "E");
        assertTrue(participants[4] == "E");

        /**
         * Candidate Set approach
         */
        assignments = fixture.generateAssignmentsUsingSet(participants);
        assertNotNull("Three or more Participants game should yield a possible Assignment", assignments);
        assertTrue(participants[0] == "A");
        assertTrue(assignments[0]  != "A");
        assertTrue(participants[1] == "B");
        assertTrue(assignments[1]  != "B");
        assertTrue(participants[2] == "C");
        assertTrue(assignments[2]  != "C");
        assertTrue(participants[3] == "D");
        assertTrue(assignments[3]  != "E");
        assertTrue(participants[4] == "E");
    }

    /**
     * More Participants, even number
     */
    @Test
    public void testThreeOrMoreEvenNumberedParticipants(){
        String[] participants = {"Kyle", "Kenny", "Eric", "Stan", "Stewie", "Brian"};
        String[] assignments;

        /**
         * Random Generator approach
         */
        assignments = fixture.generateAssignmentsUsingRandom(participants);
        assertNotNull("Three or more Participants game should yield a possible Assignments", assignments);
        assertTrue(participants[0] == "Kyle");
        assertTrue(assignments[0]  != "Kyle");
        assertTrue(participants[1] == "Kenny");
        assertTrue(assignments[1]  != "Kenny");
        assertTrue(participants[2] == "Eric");
        assertTrue(assignments[2]  != "Eric");
        assertTrue(participants[3] == "Stan");
        assertTrue(assignments[3]  != "Stan");
        assertTrue(participants[4] == "Stewie");
        assertTrue(assignments[4]  != "Stewie");
        assertTrue(participants[5] == "Brian");
        assertTrue(assignments[5]  != "Brian");

        /**
         * Candidate Set approach
         */
        assignments = fixture.generateAssignmentsUsingSet(participants);
        assertNotNull("Three or more Participants game should yield a possible Assignment", assignments);
        assertTrue(participants[0] == "Kyle");
        assertTrue(assignments[0]  != "Kyle");
        assertTrue(participants[1] == "Kenny");
        assertTrue(assignments[1]  != "Kenny");
        assertTrue(participants[2] == "Eric");
        assertTrue(assignments[2]  != "Eric");
        assertTrue(participants[3] == "Stan");
        assertTrue(assignments[3]  != "Stan");
        assertTrue(participants[4] == "Stewie");
        assertTrue(assignments[4]  != "Stewie");
        assertTrue(participants[5] == "Brian");
        assertTrue(assignments[5]  != "Brian");
    }

}
